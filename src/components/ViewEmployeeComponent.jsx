import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService';
import HeaderAdmin from './HeaderAdmin';


class ViewEmployeeComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            employee: {}
        };
    }

    componentDidMount () {
        EmployeeService.getEmployeeById(this.state.id).then(res => {
            this.setState({ employee: res.data });
        });
    }
    cancel () {
        this.props.history.push('/employees');
    }

    render () {
        return (
            <> <HeaderAdmin history={this.props.history} />
                <div style={{ paddingTop: '150px' }}>
                    <br></br>
                    <div className="card col-md-6 offset-md-3">
                        <h3 className="text-center"> Agents Details</h3>
                        <div className="card-body">
                            <div className="row">
                                <label> Nom :&nbsp; </label>
                                <div> {this.state.employee.firstName}</div>
                            </div>
                            <div className="row">
                                <label> Prénom : &nbsp;</label>
                                <div> {this.state.employee.lastName}</div>
                            </div>
                            <div className="row">
                                <label>Email :&nbsp; </label>
                                <div> {this.state.employee.emailId}</div>
                            </div>
                            <div className="row">
                                <label> cin :&nbsp; </label>
                                <div> {this.state.employee.cin}</div>
                            </div>
                            <div className="row">
                                <label> Date de naissance : &nbsp;</label>
                                <div> {this.state.employee.naissance}</div>
                            </div>
                            <div className="row">
                                <label>adresse :&nbsp; </label>
                                <div> {this.state.employee.adresse}</div>
                            </div>
                            <div className="row">
                                <label>Numéro de matriculation :&nbsp; </label>
                                <div> {this.state.employee.numeromatriculation}</div>
                            </div>
                            <div className="row">
                                <label>Numéro de téléphone  : :&nbsp; </label>
                                <div> {this.state.employee.numerotelephone}</div>
                            </div>
                            <div className="row">
                                <label>Numéro de patente  : :&nbsp; </label>
                                <div> {this.state.employee.numerotelephone}</div>
                            </div>


                            <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ position: 'relative', left: '0px' }}>retour</button>

                        </div>

                    </div>


                </div>
            </>
        );
    }
}

export default ViewEmployeeComponent;
