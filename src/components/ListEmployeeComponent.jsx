import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService';
import HeaderAdmin from './HeaderAdmin';

class ListEmployeeComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            employees: []
        };
        this.addEmployee = this.addEmployee.bind(this);
        this.editEmployee = this.editEmployee.bind(this);
        this.deleteEmployee = this.deleteEmployee.bind(this);
    }

    deleteEmployee (id) {
        EmployeeService.deleteEmployee(id).then(res => {
            this.setState({ employees: this.state.employees.filter(employee => employee.id !== id) });
        });
    }
    viewEmployee (id) {
        this.props.history.push(`/view-employee/${ id }`);
    }
    editEmployee (id) {
        this.props.history.push(`/add-employee/${ id }`);
    }

    componentDidMount () {
        EmployeeService.getEmployees().then((res) => {
            this.setState({ employees: res.data });
        });
    }

    addEmployee () {
        this.props.history.push('/add-employee/_add');
    }

    render () {
        return (
            <> <HeaderAdmin history={this.props.history} />
                <div style={{ paddingTop: '150px' }}>
                    <h2 className="text-center"> La liste des agents</h2>
                    <div className="row">
                        <button className="btn btn-primary" onClick={this.addEmployee}>Ajouter Agent</button>
                    </div>
                    <br></br>
                    <div className="row">
                        <table className="table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Nom </th>
                                    <th>  Prénom</th>
                                    <th> Email</th>
                                    <th> cin </th>
                                    <th>  Numero de telephone</th>
                                    <th> adresse</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.employees.map(
                                        employee =>
                                            <tr key={employee.id}>
                                                <td> {employee.firstName} </td>
                                                <td> {employee.lastName}</td>
                                                <td> {employee.emailId}</td>
                                                <td> {employee.cin} </td>
                                                <td> {employee.numerotelephone}</td>
                                                <td> {employee.adresse}</td>


                                                <td>
                                                    <button onClick={() => this.editEmployee(employee.id)} className="btn btn-info">Modifier </button>
                                                    <button style={{ marginLeft: "10px" }} onClick={() => this.deleteEmployee(employee.id)} className="btn btn-danger">Supprimer </button>
                                                    <button style={{ marginLeft: "10px" }} onClick={() => this.viewEmployee(employee.id)} className="btn btn-info">details </button>
                                                </td>
                                            </tr>
                                    )
                                }
                            </tbody>
                        </table>

                    </div>


                </div>
            </>
        );
    }
}

export default ListEmployeeComponent;
