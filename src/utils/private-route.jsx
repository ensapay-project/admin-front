// import { useKeycloak } from "@react-keycloak/web";
import { Redirect, Route } from 'react-router-dom';
import React from 'react';
import { isAuthenticated } from '../services/auth-service';
export const PrivateRoute = ({ component: Component, ...props }) => {
      return <Route
            {...props}
            render={props => isAuthenticated ? <Component {...props} /> : <Redirect to={{ pathname: '/' }} />}
      />;
};