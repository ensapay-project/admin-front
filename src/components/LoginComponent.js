import React, { Component } from 'react'
import "bootstrap/dist/css/bootstrap.css";
import {Button, Alert, Row, Col} from 'react-bootstrap';
import LeftSide from "./LeftSide";
import RightSide from "./RightSide";
import HeaderComponent from './HeaderComponent';
import { isAuthenticated } from '../services/auth-service';
import { Redirect } from 'react-router-dom';


class LoginComponent extends Component {

  componentDidMount () {

    if (isAuthenticated)
        return <Redirect to={{pathname: '/employees'}} />
  }
  
  render () {
        if (isAuthenticated)
        return <Redirect to={{pathname: '/employees'}} />
        return (
           <>
          <HeaderComponent/>
    <div className="App">
      
      <Row className="landing">
        <Col ><LeftSide history={this.props.history} /></Col>
       <Col style={{width:"100%"}} ><RightSide /></Col>
      </Row>
   
    </div>
    </>    
        )
    }
}

export default LoginComponent
