import axios from "axios";

import interceptor from '../utils/interceptor';

const keycloak = {
  url: "http://auth-server:8080/auth",
  realm: "ensapay",
  clientId: "resource-server",
};
 const     AUTH_SERVER_URI = `${ keycloak.url }/realms/${ keycloak.realm }/protocol`;

const init = () => {
         const authData = new URLSearchParams();
            authData.append('grant_type', 'password');
            authData.append('client_id', keycloak.clientId);
            return authData;
}

export const handleLogin = (username, password) => {
  const data = init();
            data.append('username', username);
            data.append('password', password);
            return axios.post(`${ AUTH_SERVER_URI }/openid-connect/token`, data,
                  { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                  .then(response => {
                        console.log(response);
                        localStorage.setItem('access_token', response.data.access_token);
                  })
                  .catch(error => console.log("error login", error));
}

export const handleLogout = (history) => {
      localStorage.removeItem("access_token");
      // hashHistory.push("/");
      window.location.reload();
}

export const token = localStorage.getItem("access_token");

export const isAuthenticated = token !== undefined && token !== null && token.length > 0;

export const fetchUser = () => interceptor.get(`${ AUTH_SERVER_URI }/openid-connect/userinfo`,);