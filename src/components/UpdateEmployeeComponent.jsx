import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService';
import HeaderAdmin from './HeaderAdmin';

class UpdateEmployeeComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            emailId: '',
            cin: '',
            datenaissance: '',
            adresse: '',
            numeromatriculation: '',
            numerotelephone: '',
            numeropatente: '',
            file: null
        };
        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.changefileHandler = this.changefileHandler.bind(this);
        this.changecinHandler = this.changecinHandler.bind(this);
        this.changedatenaissanceHandler = this.changedatenaissanceHandler.bind(this);

        this.changenumeromatriculationHandler = this.changenumeromatriculationHandler.bind(this);
        this.changenumerotelephoneHandler = this.changenumerotelephoneHandler.bind(this);
        this.changenumeropatenteHandler = this.changenumeropatenteHandler.bind(this);
        this.updateEmployee = this.updateEmployee.bind(this);
    }

    componentDidMount () {
        EmployeeService.getEmployeeById(this.state.id).then((res) => {
            let employee = res.data;
            this.setState({
                firstName: employee.firstName,
                lastName: employee.lastName,
                emailId: employee.emailId,
                file: employee.file,
                cin: employee.cin,
                datenaissance: employee.datenaissance,
                adresse: employee.adresse,
                numeromatriculation: employee.numeromatriculation,
                numerotelephone: employee.numerotelephone,
                numeropatente: employee.numeropatente,
            });
        });
    }

    updateEmployee = (e) => {
        e.preventDefault();
        let employee = {
            firstName: this.state.firstName, lastName: this.state.lastName, emailId: this.state.emailId, file: this.state.file, cin: this.state.cin,
            datenaissance: this.state.datenaissance,
            adresse: this.state.adresse,
            numeromatriculation: this.state.numeromatriculation,
            numerotelephone: this.state.numerotelephone,
            numeropatente: this.state.numeropatente
        };
        console.log('employee => ' + JSON.stringify(employee));
        console.log('id => ' + JSON.stringify(this.state.id));
        EmployeeService.updateEmployee(employee, this.state.id).then(res => {
            this.props.history.push('/employees');
        });
    };

    changeFirstNameHandler = (event) => {
        this.setState({ firstName: event.target.value });
    };

    changeLastNameHandler = (event) => {
        this.setState({ lastName: event.target.value });
    };

    changeEmailHandler = (event) => {
        this.setState({ emailId: event.target.value });
    };
    changefileHandler = (event) => {
        this.setState({ file: event.target.files[ 0 ] });
    };

    changecinHandler = (event) => {
        this.setState({ cin: event.target.value });
    };
    changedatenaissanceHandler = (event) => {
        this.setState({ datenaissance: event.target.value });
    };
    changeadresseHandler = (event) => {
        this.setState({ adresse: event.target.value });
    };
    changenumeromatriculationHandler = (event) => {
        this.setState({ numeromatriculation: event.target.value });
    };
    changenumerotelephoneHandler = (event) => {
        this.setState({ numerotelephone: event.target.value });
    };
    changenumeropatenteHandler = (event) => {
        this.setState({ numeropatente: event.target.value });
    };

    cancel () {
        this.props.history.push('/employees');
    }

    render () {
        return (
            <> <HeaderAdmin history={this.props.history} />
                <div style={{ paddingTop: '150px' }}>
                    <br></br>
                    <div className="container" style={{ paddingTop: '150px' }}>
                        <div className="row">
                            <div className="card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Modifier agent</h3>
                                <div className="card-body">
                                    <form>
                                        <div className="form-group">
                                            <label> Nom </label>
                                            <input placeholder="Nom" name="firstName" className="form-control"
                                                value={this.state.firstName} onChange={this.changeFirstNameHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label>Prénom </label>
                                            <input placeholder="Prenom" name="lastName" className="form-control"
                                                value={this.state.lastName} onChange={this.changeLastNameHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label> Email  </label>
                                            <input placeholder="Email Address" name="emailId" className="form-control"
                                                value={this.state.emailId} onChange={this.changeEmailHandler} />
                                        </div>

                                        <div className="form-group">
                                            <label> cin </label>
                                            <input placeholder="cin" name="cin" className="form-control"
                                                value={this.state.cin} onChange={this.changecinHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label> Date de naissance </label>
                                            <input placeholder="datenaissance" name="datenaissance" className="form-control"
                                                value={this.state.datenaissance} onChange={this.changedatenaissanceHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label>  </label>
                                            <input placeholder="Num" name="adresse" className="form-control"
                                                value={this.state.numeromatriculation} onChange={this.changenumeromatriculationHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label> Numéro de télephone </label>
                                            <input placeholder="Numero telephone" name="numerotelephone" className="form-control"
                                                value={this.state.numerotelephone} onChange={this.changenumerotelephoneHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label> Numéro de patente </label>
                                            <input placeholder="Numero patente" name="numeropatente" className="form-control"
                                                value={this.state.numeropatente} onChange={this.changenumeropatenteHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label> Upload file cin : &nbsp;</label>
                                            <input type="file" onChange={this.changefileHandler} />
                                        </div>

                                        <div className="form-group">
                                            <label>  </label>
                                            <input placeholder="Address" name="adresse" className="form-control"
                                                value={this.state.adresse} onChange={this.changeadresseHandler} />
                                        </div>


















                                        <button className="btn btn-success" onClick={this.updateEmployee}>enregistrer</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10px" }}>annuler</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </>
        );
    }
}

export default UpdateEmployeeComponent;
