import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { handleLogin as login } from '../services/auth-service';

const LeftSide = ({ keycloak, history }) => {
    const [ username, setUsername ] = useState();
    const [ password, setPassword ] = useState();

    // const router = useRouter();

    const handleLogin = (e) => {
        e.preventDefault();
        login(username, password).then(() => { history.push('/employees'); console.log('pushed'); window.location.reload(); });

    };

    return (
        <div style={{ paddingTop: '150px' }} >

            <Form onSubmit={e => handleLogin(e)} style={{ width: "60%", marginLeft: "0%", marginTop: "20%", borderRadius: "40px", textAlign: "left" }}>
                <Form.Group >
                    <div style={{ fontFamily: 'fredokaOne', marginBottom: "50px", fontSize: "30px" }} > Espace d'administration</div>

                    <Form.Label >Entrer votre email</Form.Label>
                    <Form.Control type="text" onChange={(e) => setUsername(e.target.value)} placeholder="Entrer votre email" />
                </Form.Group>
                <Form.Group >
                    <Form.Label>Entrer votre mot de passe</Form.Label>
                    <Form.Control type="password" onChange={(e) => setPassword(e.target.value)} placeholder="Entrer votre mot de passe" />
                </Form.Group>
                <Button type="submit" >Connecter</Button>
            </Form>
        </div>
    );
};

export default LeftSide;