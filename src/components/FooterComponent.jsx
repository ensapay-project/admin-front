import React, { Component } from 'react'
import { MDBContainer,  MDBFooter } from "mdbreact";

class FooterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <MDBFooter color="blue" className="font-small pt-4 mt-4" style={{fontFamily:'fredokaOne', bottom: '0',left: '0',position: 'relative',width:'100%'}}>
      <div className="footer-copyright text-center py-3" style={{fontFamily:'fredokaOne'}}>
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: <a href="#" style={{fontFamily:'fredokaOne',marginBottom:'0%'}}> ENSAPAY.com </a>
        </MDBContainer>
      </div>
    </MDBFooter>
        )
    }
}

export default FooterComponent
