import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ListEmployeeComponent from './components/ListEmployeeComponent';
import CreateEmployeeComponent from './components/CreateEmployeeComponent';
import UpdateEmployeeComponent from './components/UpdateEmployeeComponent';
import ViewEmployeeComponent from './components/ViewEmployeeComponent';
import LoginComponent from './components/LoginComponent';
import FooterComponent from './components/FooterComponent';
import {PrivateRoute} from './utils/private-route';
import { ReactKeycloakProvider } from '@react-keycloak/web'
 
import keycloak from './keycloak';

function App() {
  return (
    // <ReactKeycloakProvider authClient={keycloak}>
    <div>
        <Router>
       
                <div className="container">
                    <Switch> 
                          <Route path = "/" exact component = {LoginComponent}></Route>
                          <PrivateRoute path = "/employees" component = {ListEmployeeComponent}></PrivateRoute>
                          <PrivateRoute path = "/add-employee/:id" component = {CreateEmployeeComponent}></PrivateRoute>
                          <PrivateRoute path = "/view-employee/:id" component = {ViewEmployeeComponent}></PrivateRoute>  
                          {/* <Route path = "/update-employee/:id" component = {UpdateEmployeeComponent}></Route> */}
                    </Switch>
                </div>
                <FooterComponent/> 
        </Router>
      </div>
      // </ReactKeycloakProvider>
    
  );
}

export default App;

