import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService';
import HeaderAdmin from './HeaderAdmin';




class CreateEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            emailId: '',
            cin:'',
            datenaissance: '',
            adresse: '',
            numeromatriculation: '',
            numerotelephone:'',
            numeropatente:'',
             file :null
        }
        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.changeEmailHandler =  this.changeEmailHandler.bind(this);
        this.changefileHandler = this.changefileHandler.bind(this);
        this.changecinHandler = this.changecinHandler.bind(this);
        this.changedatenaissanceHandler =  this.changedatenaissanceHandler.bind(this);

        this.changenumeromatriculationHandler = this.changenumeromatriculationHandler.bind(this);
        this.changenumerotelephoneHandler = this.changenumerotelephoneHandler.bind(this);
        this.changenumeropatenteHandler =  this.changenumeropatenteHandler.bind(this);



        
        this.saveOrUpdateEmployee = this.saveOrUpdateEmployee.bind(this);
        
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            EmployeeService.getEmployeeById(this.state.id).then( (res) =>{
                let employee = res.data;
                this.setState({firstName: employee.firstName,
                    lastName: employee.lastName,
                    emailId : employee.emailId,
                    file : employee.file,
                    cin:employee.cin,
                    datenaissance: employee.datenaissance,
                    adresse: employee.adresse,
                    numeromatriculation: employee.numeromatriculation,
                    numerotelephone:employee.numerotelephone,
                    numeropatente:employee.numeropatente
                });
            });
        }        
    }
    saveOrUpdateEmployee = (e) => {
        e.preventDefault();
        let employee = {firstName: this.state.firstName, lastName: this.state.lastName, emailId: this.state.emailId, file: this.state.file , cin: this.state.cin,
        datenaissance:  this.state.datenaissance,
        adresse:  this.state.adresse,
        numeromatriculation:  this.state.numeromatriculation,
        numerotelephone: this.state.numerotelephone,
        numeropatente: this.state.numeropatente};
        const data = new FormData();
        //using File API to get chosen file
        data.append('file', this.state.file);
        data.append('firstName', this.state.firstName);
        data.append('lastName', this.state.lastName); 
        data.append('emailId', this.state. emailId);  
        data.append('cin', this.state.cin);
        data.append('datenaissance', this.state.datenaissance);
        data.append('adresse', this.state.adresse); 
        data.append('numeromatriculation', this.state.numeromatriculation);    
        data.append('numerotelephone', this.state.numerotelephone);
        data.append('numeropatente', this.state.numeropatente);
        
       ;  console.log('employee => ' + JSON.stringify(employee));

        // step 5
        if(this.state.id === '_add'){
            EmployeeService.createEmployee(data).then(res =>{
                this.props.history.push('/employees');
            });
        }else{
            EmployeeService.updateEmployee(data, this.state.id).then( res => {
                this.props.history.push('/employees');
            });
        }
    }
    
    changeFirstNameHandler= (event) => {
        this.setState({firstName: event.target.value});
    }

    changeLastNameHandler= (event) => {
        this.setState({lastName: event.target.value});
    }

    changeEmailHandler= (event) => {
        this.setState({emailId: event.target.value});
    }
    changefileHandler= (event) => {
        this.setState({file: event.target.files[0]});
    }

    changecinHandler= (event) => {
        this.setState({cin: event.target.value});
    }
    changedatenaissanceHandler= (event) => {
        this.setState({datenaissance: event.target.value});
    }  
    changeadresseHandler= (event) => {
        this.setState({adresse: event.target.value});
    }  
    changenumeromatriculationHandler= (event) => {
        this.setState({numeromatriculation: event.target.value});
    }  
    changenumerotelephoneHandler= (event) => {
        this.setState({numerotelephone: event.target.value});
    }  
    changenumeropatenteHandler= (event) => {
        this.setState({numeropatente: event.target.value});
    }  


    cancel(){
        this.props.history.push('/employees');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Ajouter Agent</h3>
        }else{
            return <h3 className="text-center">modifier Agent</h3>
        }
    }
    render() {
        return (
            <> <HeaderAdmin/> 
            <div style={{paddingTop:'150px'}}>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Nom: </label>
                                            <input placeholder="Nom" name="firstName" className="form-control" 
                                                value={this.state.firstName} onChange={this.changeFirstNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Prenom: </label>
                                            <input placeholder="Prenom" name="lastName" className="form-control" 
                                                value={this.state.lastName} onChange={this.changeLastNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Email : </label>
                                            <input placeholder="Email Address" name="emailId" className="form-control" 
                                                value={this.state.emailId} onChange={this.changeEmailHandler}/>
                                        </div>
                                     
                                        <div className = "form-group">
                                            <label>  Numéro Cin : </label>
                                            <input placeholder="Numéro de cin" name="cin" className="form-control" 
                                                value={this.state.cin} onChange={this.changecinHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Date de naissance : </label>
                                            <input placeholder="Date de naissance " name="datenaissance" className="form-control" 
                                                value={this.state.datenaissance} onChange={this.changedatenaissanceHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Numéro de téléphone : </label>
                                            <input placeholder="Numéro de téléphone " name="numerotelephone" className="form-control" 
                                                value={this.state.numerotelephone} onChange={this.changenumerotelephoneHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Numéro de matriculation : </label>
                                            <input placeholder="Numéro de matriculation " name="numeromatriculation" className="form-control" 
                                                value={this.state.numeromatriculation} onChange={this.changenumeromatriculationHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Numéro de patente  : </label>
                                            <input placeholder="Numéro de patente " name="numeropatente" className="form-control" 
                                                value={this.state.numeropatente} onChange={this.changenumeropatenteHandler}/>
                                        </div>
                                        
                                     <div className = "form-group">
                                            <label> Upload file cin : &nbsp;</label> 
                                            <input type="file"  onChange={this.changefileHandler} />
                                        </div>
                                      
                                        <button className="btn btn-success" onClick={this.saveOrUpdateEmployee}>enregistrer</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>annuler</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
            </>
        )
    }
}

export default CreateEmployeeComponent
