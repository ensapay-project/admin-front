import React, { Component } from 'react'
import { Navbar, Nav, NavDropdown, Container,Button } from 'react-bootstrap';
import bank from "../images/bank.png"
import { handleLogout,fetchUser } from '../services/auth-service';



class HeaderAdmin extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 name:''
        }
    }
  
  componentDidMount () {
    fetchUser().then(response => {
      this.setState({ name: response.data.name })
    })
  }
  
  logout = () => handleLogout(this.props.history);
    

    render() {
        return (
            <div >
            <Navbar style={{backgroundColor:"whitesmoke",width:"100%",left: '0',position: 'absolute'}}>
            <Navbar.Brand ><img src={bank} style={{width: "50px",height: "50px"}}/></Navbar.Brand>
  
    <Navbar.Brand href="#" style={{fontFamily:'fredokaOne'}}>ENSAPAY</Navbar.Brand>
    <Navbar.Toggle />
  <Navbar.Collapse className="justify-content-end">
    <Navbar.Text>
      {this.state.name} <Button onClick={this.logout.bind(this)}> Déconnexion</Button>
    </Navbar.Text>
  </Navbar.Collapse>
  
</Navbar>
        </div>
        )
    }
}

export default HeaderAdmin
