import React from 'react';
import {Image} from "react-bootstrap";
import money from '../images/money.jpg';

const RightSide = () => {
    return (
        <div style={{paddingTop:'150px'}} > 
           <Image src={money}  style={{border:"none",borderRadius:"20px",height:"auto", width:"150%", marginTop:"40px"}} /> 
        </div>
    )
}

export default RightSide;