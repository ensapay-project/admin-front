import axios from 'axios';
class Service {

  constructor() {
    console.log("Service is constructed");
  }

  getRestClient() {
    if (!this.serviceInstance) {
      this.serviceInstance = axios.create({
        baseURL: 'http://localhost:8080/api/v1/employees/{id}',
        timeout: 10000,
        headers: {
            'Content-Type': 'application/json'
          },
      });
    }
    return this.serviceInstance;
  }
  getRestClientUpload() {
    if (!this.serviceInstance) {
      this.serviceInstance = axios.create({
        baseURL: 'http://localhost:8080/api/v1/employees/{id}',
        timeout: 10000,
        headers: {
            'Content-Type': 'multipart/form-data'
          },
      });
    }
    return this.serviceInstance;
  }
}

export default (new Service());