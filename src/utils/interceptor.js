import axios from 'axios';
import { isAuthenticated, token } from '../services/auth-service';

const interceptor = axios.create();
interceptor.interceptors.request.use(
  config => {
    if (isAuthenticated)
            config.headers.Authorization = "Bearer " + token;
    return config;
  },
  error => {
    Promise.reject(error)
      });

export default interceptor;